using System.Collections;
using System.Collections.Generic;
using System.IO;

using UnityEngine;
using UnityEditor;
using UnityEngine.Networking;
using UnityEngine.Rendering;

// convenience class for objects that have an attached WebVideoPlayer
public class WebVideoObject : MonoBehaviour
{
    InternalWebVideoPlayer webVideoPlayer;
    public string url;
    public float distanceToPlay = 10;
    public bool muted = false;
    public float minAudioDistance = 8;
    public float maxAudioDistance = 12;
    public int width = 1920;
    public int height = 1080;
    public bool useAttachedAudioSource = false;
    string materialName = "videomat";
    bool isActive = false;

    // store local position, to calculate distance to player
    Vector3 localPosition;

    public GameObject player;
    public GameObject[] mirrors;

    // do we need this?
    //public void SetSpatial(Vector3 position, Vector3 scale, Quaternion rotation) {
        //localPosition = position;
        //gameObject.transform.position = position;
        //gameObject.transform.localScale = scale;
        //gameObject.transform.localRotation = rotation;
        //webVideoPlayer.audioSource.transform.position = position;
    //}

    public void Start() {
        Debug.Log("Start webVideoPlayer");
        localPosition = gameObject.transform.position;
        // audioSource = gameObject.AddComponent<AudioSource>();

        if (GraphicsSettings.defaultRenderPipeline) {
            if (GraphicsSettings.defaultRenderPipeline.name == "UniversalRenderPipelineAsset") {
                materialName = "videomat_urp";
            }
        }

        // check if someone forgot to add the webm extension
        if (!url.Contains(".webm")) {
            if (!url.Contains(".")) {
                url = url + ".webm";
            }
        }
#if UNITY_EDITOR
        Debug.Log("webVideoPlayer copying resources");

        // copy Resources
        if (!AssetDatabase.IsValidFolder("Assets/Resources")) {
            AssetDatabase.CreateFolder("Assets", "Resources");
        }
        if (!AssetDatabase.IsValidFolder("Assets/Resources/Materials")) {
            AssetDatabase.CreateFolder("Assets/Resources", "Materials");
        }
        var materialArray = AssetDatabase.FindAssets(materialName, new[] {"Assets/Resources/Materials"});
        if (materialArray.Length <= 0){
            FileUtil.CopyFileOrDirectory("Assets/WebVideoPlayer/Resources/Materials/" + materialName + ".mat", "Assets/Resources/Materials/" + materialName + ".mat");

            AssetDatabase.Refresh();
        }

        Debug.Log("webVideoPlayer copying streamingassets");
        // copy StreamingAssets
        if (!AssetDatabase.IsValidFolder("Assets/StreamingAssets")) {
            FileUtil.CopyFileOrDirectory("Assets/WebVideoPlayer/StreamingAssets", "Assets/StreamingAssets");
            AssetDatabase.Refresh();
        }

#endif
        Debug.Log("webVideoPlayer setting material");

        Material referenceMaterial = Resources.Load<Material>("Materials/" + materialName);
        //Material referenceMaterial = GraphicsSettings.defaultRenderPipeline.defaultMaterial;

        gameObject.GetComponent<Renderer>().material.CopyPropertiesFromMaterial(referenceMaterial);

        foreach(GameObject mirror in mirrors) {
            mirror.GetComponent<Renderer>().material = gameObject.GetComponent<Renderer>().material;
        }

        // if there is no player set, look for FirstPerson-AIO
        if (!player) {
            player = GameObject.Find("FirstPerson-AIO");
        }
        if (!player) {
        // if there is still no player set, look for Super First Person
            player = GameObject.Find("Super First Person Controller");
        }
        // Alright, and if there is still no player set, maybe there is a main camera
        if (!player) {
            player = Camera.main.gameObject;
        }
        initWebPlayer();
    }

    public void initWebPlayer() {
        webVideoPlayer = gameObject.AddComponent<InternalWebVideoPlayer>();
        webVideoPlayer.useAttachedAudioSource = useAttachedAudioSource;
        if (!useAttachedAudioSource) {
            webVideoPlayer.audioSource = gameObject.AddComponent<AudioSource>();
            webVideoPlayer.audioSource.dopplerLevel = 0;
            webVideoPlayer.audioSource.spatialize = true;
            webVideoPlayer.audioSource.spatialBlend = 1;
            webVideoPlayer.audioSource.minDistance = minAudioDistance;
            webVideoPlayer.audioSource.maxDistance = maxAudioDistance;
            webVideoPlayer.audioSource.rolloffMode = AudioRolloffMode.Linear;
        }
        webVideoPlayer.url = url;
        webVideoPlayer.width = width;
        webVideoPlayer.height = height;
        webVideoPlayer.material = gameObject.GetComponent<Renderer>().material;

        webVideoPlayer.Start();

        Debug.Log("initialized webVideoPlayer");
    }

    public void Update()
    {
        // if there is a player, get the distance to it.
        // otherwise we set a negative distance which should just set it active whenever possible
        float dist = player ? Vector3.Distance(localPosition, player.transform.position) : -1;
        bool shouldBeActive = dist < distanceToPlay;
        if (isActive) {
            if (!shouldBeActive) {
                webVideoPlayer.DestroyMe();
                webVideoPlayer.shouldBeRunning = false;
            } else {
                if (!useAttachedAudioSource && muted != webVideoPlayer.audioSource.mute) {
                    webVideoPlayer.audioSource.mute = muted;
                }
                webVideoPlayer.Update();
                webVideoPlayer.shouldBeRunning = true;
            }
        }
        isActive = shouldBeActive;
    }
}
