# WebVideoPlayer

## preparation before opening unity
1. copy WebVideoPlayer directory to Assets/WebVideoPlayer
2. make sure your video is in webm format. You can use the handbrake_preset.json in the WebVideoPlayer folder to convert your video. 
3. put your video in the Assets/StreamingAssets directory, create the directory if you need

## within Unity
1. Import FirstPerson-AIO (First Person All-In-One in the Asset Store)
2. create a 3D Object (e.g. Cube or Sphere) to show the Video on
3. attach the "Assets/WebVideoPlayer/Scripts/WebVideoObject.cs" script to the 3D Object (drag and drop, or "add Component" and search for it) 
4. In the Inspector, fill in the filename of your video in StreamingAssets under "Url"
5. Adjust "Distance To Play" to the distance you want your player to trigger the video to play
6. Adjust "Width" and "Height" to the pixel dimensions of your video
7. That's it! It should work now

## Changes to last version
- No need to drag the "videomat" material on the 3D Object manually
- You can leave away the ".webm" extension in the filename (optional)
- Should work with all Unity versions (2019, 2020 and 2021)


If anything is unclear, don't be afraid to ask!
